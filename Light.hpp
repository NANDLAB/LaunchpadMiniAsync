#ifndef LIGHT_HPP
#define LIGHT_HPP

#include <vector>
#include <cstdint>
#include <memory>

namespace novation {

/*
 * A light variable can be created like this:
 * ConstLightPtr l (new LightRGB(80, 90, 30));
 *
 */

/**
 * @brief Immutable class representing light
 */
class Light
{
protected:
    std::uint8_t type;
    std::vector<std::uint8_t> data;
    Light() = delete;
    Light(std::uint8_t type, std::vector<std::uint8_t> data);
    Light(const Light &rhs) = default;
    Light(Light &&rhs) = default;

public:
    ~Light() = default;

    enum : std::uint8_t {
        STATIC,
        FLASHING,
        PULSING,
        RGB
    };

    std::uint8_t get_type() const;
    std::vector<std::uint8_t> get_data() const;
};

typedef const Light ConstLight;

typedef std::shared_ptr<Light> LightPtr;
typedef std::shared_ptr<ConstLight> ConstLightPtr;

/**
 * @brief Static light
 */
struct LightStatic : public Light {
    LightStatic(std::uint8_t palette);
};

typedef const LightStatic ConstLightStatic;

/**
 * @brief Flashing: b a b a ...
 */
struct LightFlashing : public Light {
    /**
     * @brief LightStatic
     * b and a are both palette colors.
     * @param b
     * @param a
     */
    LightFlashing(std::uint8_t b, std::uint8_t a);
};

typedef const LightFlashing ConstLightFlashing;

/**
 * @brief Pulsing light
 */
struct LightPulsing : public Light {
    LightPulsing(std::uint8_t palette);
};

typedef const LightPulsing ConstLightPulsing;

/**
 * @brief Light specified by RGB values
 */
struct LightRGB : public Light {
    LightRGB(std::uint8_t r,
             std::uint8_t g,
             std::uint8_t b);
};

typedef const LightRGB ConstLightRGB;

}

#endif // LIGHT_HPP
