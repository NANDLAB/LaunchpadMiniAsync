#ifndef PAD_HPP
#define PAD_HPP

#include <cstdint>

namespace novation {

struct Pad
{
    std::uint8_t x;
    std::uint8_t y;
    Pad() = default;
    Pad(std::uint8_t x, std::uint8_t y)
        : x(x), y(y)
    {}
    Pad(std::uint8_t padnum)
        : x(padnum % 10 - 1),
          y(padnum / 10 - 1)
    {}
    Pad(const Pad &rhs) = default;
    Pad(Pad &&rhs) = default;
    ~Pad() = default;
    std::uint8_t padnum() const
    { return 10 * y + x + 11; }
};

}

#endif // PAD_HPP
