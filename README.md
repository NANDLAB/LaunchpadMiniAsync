# LaunchpadMiniAsync

C++ library for the Novation Launchpad Mini MIDI controller with asynchronous event handling. It uses the [RtMidi](https://github.com/thestk/rtmidi) and [Boost ASIO](https://www.boost.org/doc/libs/1_78_0/doc/html/boost_asio.html) library.

# Build Requirements
You need CMake to build this library. Install it with:
```
sudo apt install cmake
```

Install Boost library with APT:
```
sudo apt install libboost-system-dev
```

## RtMidi
The RtMidi library can be either installed from the APT repository or build manually from source.
- On the Raspberry Pi RtMidi is well maintained, so it is recommended to install it with apt.
- On other systems (e.g. Linux Mint) it can be outdated, so it might be better to build the latest version yourself.

### Install RtMidi With APT
```
sudo apt install librtmidi-dev
```

### Build And Install RtMidi Manually
First clone the [RtMidi](https://github.com/thestk/rtmidi) git repository somewhere:
```
git clone https://github.com/thestk/rtmidi.git
cd rtmidi
```
Enter the following commands to build and install RtMidi:
```
./autogen.sh
make
sudo make install
```

# Build LaunchpadMiniAsync
If LaunchpadMiniAsync is used as a git subproject, ignore this step. This creates the library archive `libLaunchpadMiniAsync.a`.
```
cmake ..
cmake --build .
```
