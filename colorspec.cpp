#include "colorspec.hpp"
#include <cassert>

namespace novation {

std::vector<std::uint8_t> make_pad_colorspec(Pad pad, ConstLightPtr light) {
    std::vector<std::uint8_t> v = {light->get_type()};
    v.push_back(pad.padnum());
    std::vector<std::uint8_t> data = light->get_data();
    v.insert(v.end(), data.cbegin(), data.cend());
    return v;
}

std::vector<std::uint8_t> make_text_colorspec(ConstLightPtr light) {
    std::uint8_t type = light->get_type();
    assert(type == Light::STATIC || type == Light::RGB);
    std::vector<std::uint8_t> v = {std::uint8_t(type == Light::RGB)};
    std::vector<std::uint8_t> data = light->get_data();
    v.insert(v.end(), data.cbegin(), data.cend());
    return v;
}

}
