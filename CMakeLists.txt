cmake_minimum_required(VERSION 3.6)

project(LaunchpadMiniAsync LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(THREADS_PREFER_PTHREAD_FLAG ON)

add_library(LaunchpadMiniAsync STATIC
  LaunchpadMiniAsync.cpp
  LaunchpadMiniAsync.hpp
  Light.cpp
  Light.hpp
  Pad.hpp
  colorspec.cpp
  colorspec.hpp
)

find_package(PkgConfig REQUIRED)
find_package(Boost REQUIRED system)
find_package(Threads REQUIRED)

pkg_check_modules(RTMIDI REQUIRED IMPORTED_TARGET rtmidi)

target_include_directories(LaunchpadMiniAsync PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")
target_link_libraries(LaunchpadMiniAsync PUBLIC PkgConfig::RTMIDI Boost::system Threads::Threads)
target_compile_definitions(LaunchpadMiniAsync PRIVATE LAUNCHPADMINIASYNC_LIBRARY)
