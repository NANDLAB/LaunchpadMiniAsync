#ifndef COLORSPEC_HPP
#define COLORSPEC_HPP

#include <vector>
#include <cstdint>
#include <Pad.hpp>
#include <Light.hpp>

namespace novation {

std::vector<std::uint8_t> make_pad_colorspec(Pad pad, ConstLightPtr light);
std::vector<std::uint8_t> make_text_colorspec(ConstLightPtr light);

}

#endif // COLORSPEC_HPP
