#include "LaunchpadMiniAsync.hpp"
#include <cassert>
#include <iostream>
#include <iomanip>
#include <ios>
#include <boost/asio/post.hpp>
#include <boost/bind.hpp>
#include <utility>
#include "colorspec.hpp"

// #define LPMINI_DEBUG

#ifdef LPMINI_DEBUG
template <typename T>
static void print_vector(const std::vector<T>& v) {
    for (size_t i = 0; i < v.size(); i++) {
        if (i) { if (i % 8 == 0) puts(""); else printf(":"); }
        printf("%02X", (unsigned) v[i]);
    }
    puts("");
}

template <typename C, typename T>
static std::basic_ostream<C>& operator << (std::basic_ostream<C>& os, const std::vector<T>& v) {
    using namespace std;
    std::ios_base::fmtflags prev_fmt = os.flags(std::ios_base::hex | std::ios_base::uppercase);
    C prev_fill = os.fill(C('0'));
    for (size_t i = 0; i < v.size(); i++) {
        if (i) { os << ((i % 16) ? C(':') : C('\n')); }
        os << setw(2) << unsigned(v[i]);
    }
    os.setf(prev_fmt);
    os.fill(prev_fill);
    return os;
}
#endif

namespace novation {

// Abbreviation for convenience.
typedef LaunchpadMiniAsync LP;

enum : std::uint8_t {
    SYSEX_START = 0xF0,
    SYSEX_END   = 0xF7,

    SYSEX_SELECT_LAYOUT     = 0x00,
    SYSEX_DAW_SETUP         = 0x01,
    SYSEX_LED_LIGHT         = 0x03,
    SYSEX_TEXT_SCROLL       = 0x07,
    SYSEX_BRIGHTNESS        = 0x08,
    SYSEX_LED_ACTIVE        = 0x09, // Called LED Sleep in the manual
    SYSEX_LED_FEEDBACK      = 0x0A,
    SYSEX_PROG_LIVE         = 0x0E,
    SYSEX_DAW_STANDALONE    = 0x10,
    SYSEX_DAW_CLEAR         = 0x12,
    SYSEX_DAW_SESSION_COLOR = 0x14
};

const std::vector<std::uint8_t> LaunchpadMiniAsync::_sysExHeader = {0xF0, 0x00, 0x20, 0x29, 0x02, 0x0D};

LaunchpadMiniAsync::LaunchpadMiniAsync(boost::asio::io_context &ioc, UserCallback_t callback,
                                       MidiErrorCallback_t inErrorCb, MidiErrorCallback_t outErrorCb)
    : _ioc(ioc), _in(), _out(), _padState{}, _userCallback(std::move(callback)),
      _inErrorCallback(std::move(inErrorCb)), _outErrorCallback(std::move(outErrorCb))
{}

void LaunchpadMiniAsync::reset() {
    using namespace std;
    setSysExProgrammerMode(true);
    setSysExLightAll(make_shared<ConstLightStatic>(0));
    // setSysExLightAll(ConstLightPtr(new LightStatic(0)));
    setSysExTextScrollOff();
    setSysExPower(true);
}

void LaunchpadMiniAsync::midiInCallbackWrapper(double timeStamp, std::vector<std::uint8_t> *message, void *self) {
    LaunchpadMiniAsync *lp = reinterpret_cast<LaunchpadMiniAsync*>(self);
    boost::asio::post(lp->_ioc, boost::bind(&LaunchpadMiniAsync::midiInCallback, lp,
                                            timeStamp, std::make_shared<std::vector<std::uint8_t>>(*message)));
}

void LaunchpadMiniAsync::midiInErrorWrapper(RtMidiError::Type type, const std::string &errorText, void *self) {
    LaunchpadMiniAsync *lp = reinterpret_cast<LaunchpadMiniAsync*>(self);
    if (lp->_inErrorCallback) {
        boost::asio::post(lp->_ioc, boost::bind(lp->_inErrorCallback,
                                                type, std::make_shared<const std::string>(errorText)));
    }
}

void LaunchpadMiniAsync::midiOutErrorWrapper(RtMidiError::Type type, const std::string &errorText, void *self) {
    LaunchpadMiniAsync *lp = reinterpret_cast<LaunchpadMiniAsync*>(self);
    if (lp->_outErrorCallback) {
        boost::asio::post(lp->_ioc, boost::bind(lp->_outErrorCallback,
                                                type, std::make_shared<const std::string>(errorText)));
    }
}

void LP::defaultInErrorCallback(RtMidiError::Type type, std::shared_ptr<const std::string> errorText) {
    throw RtMidiError(*errorText, type);
}

void LP::defaultOutErrorCallback(RtMidiError::Type type, std::shared_ptr<const std::string> errorText) {
    throw RtMidiError(*errorText, type);
}

void LaunchpadMiniAsync::setCallback(UserCallback_t callback) {
    _userCallback = std::move(callback);
}

void LaunchpadMiniAsync::setInErrorCallback(MidiErrorCallback_t callback) {
    _inErrorCallback = std::move(callback);
}

void LaunchpadMiniAsync::setOutErrorCallback(MidiErrorCallback_t callback) {
    _outErrorCallback = std::move(callback);
}

void LaunchpadMiniAsync::midiInCallback(double timeStamp, std::shared_ptr<std::vector<std::uint8_t>> msg) {
    using namespace std;
#ifdef LPMINI_DEBUG
    std::cout << "IN: " << *msg << std::endl;
#endif
    std::uint8_t msg_type = (*msg)[0] & 0xF0;
    if (msg->size() == 3 && (msg_type == NOTE_OFF || msg_type == NOTE_ON || msg_type == CONTROL_CHANGE)) {
        Pad p((*msg)[1]);
        if (p.x < COLS && p.y < ROWS) {
            _padState[p.y][p.x] = (*msg)[2];
            if (_userCallback) {
                _userCallback(p, _padState[p.y][p.x]);
            }
        }
#ifdef LPMINI_DEBUG
        else {
            std::cerr << "LP-ERROR: pad number out of bounds!" << std::endl;
        }
#endif
    }
#ifdef LPMINI_DEBUG
    else {
        std::cerr << "LP-ERROR: message unknown!" << std::endl;
    }
#endif
}

void LaunchpadMiniAsync::open(unsigned portIn, unsigned portOut) {
    // Do not ignore any MIDI messages
    _in.ignoreTypes(false, false, false);
    _in.setCallback(LaunchpadMiniAsync::midiInCallbackWrapper, this);
    _in.setErrorCallback(LaunchpadMiniAsync::midiInErrorWrapper, this);
    _out.setErrorCallback(LaunchpadMiniAsync::midiOutErrorWrapper, this);
    _in.openPort(portIn);
    _out.openPort(portOut);
}

void LaunchpadMiniAsync::close() {
    _in.closePort();
    _out.closePort();
}

bool LaunchpadMiniAsync::getPressedNone() {
    return !getPressedAny();
}

bool LaunchpadMiniAsync::getPressedAny() {
    for (int y = 0; y < LaunchpadMiniAsync::ROWS; y++) {
        for (int x = 0; x < LaunchpadMiniAsync::COLS; x++) {
            if (_padState[y][x]) {
                return true;
            }
        }
    }
    return false;
}

bool LaunchpadMiniAsync::getPressedAll() {
    for (int y = 0; y < LaunchpadMiniAsync::ROWS; y++) {
        for (int x = 0; x < LaunchpadMiniAsync::COLS; x++) {
            if (!_padState[y][x]) {
                return false;
            }
        }
    }
    return true;
}

bool LaunchpadMiniAsync::getWhitePressedNone() {
    return !getWhitePressedAny();
}

bool LaunchpadMiniAsync::getWhitePressedAny() {
    for (int y = 0; y < LaunchpadMiniAsync::ROWS - 1; y++) {
        for (int x = 0; x < LaunchpadMiniAsync::COLS - 1; x++) {
            if (_padState[y][x]) {
                return true;
            }
        }
    }
    return false;
}

bool LaunchpadMiniAsync::getWhitePressedAll() {
    for (int y = 0; y < LaunchpadMiniAsync::ROWS - 1; y++) {
        for (int x = 0; x < LaunchpadMiniAsync::COLS - 1; x++) {
            if (!_padState[y][x]) {
                return false;
            }
        }
    }
    return true;
}

void LaunchpadMiniAsync::setSysExProgrammerMode(bool value) {
    using namespace std;
    vector<std::uint8_t> msg = _sysExHeader;
    msg.insert(msg.end(), {SYSEX_PROG_LIVE, std::uint8_t(value), SYSEX_END});
#ifdef LPMINI_DEBUG
    std::cout << msg << std::endl;
#endif
    _out.sendMessage(&msg);
}

void LaunchpadMiniAsync::setLightSingle(Pad pad, ConstLightPtr light) {
    using namespace std;
    uint8_t type = light->get_type();
    vector<uint8_t> data = light->get_data();
    assert(type == Light::STATIC || type == Light::FLASHING || type == Light::PULSING);
    assert(data.size());
    vector<std::uint8_t> msg;
    if (type == Light::FLASHING) {
        assert(data.size() >= 2);
        msg = {NOTE_ON, pad.padnum(), data[1]};
#ifdef LPMINI_DEBUG
        std::cout << msg << std::endl;
#endif
        _out.sendMessage(&msg);
    }
    msg = {uint8_t(NOTE_ON | light->get_type()), pad.padnum(), data[0]};
#ifdef LPMINI_DEBUG
    std::cout << msg << std::endl;
#endif
    _out.sendMessage(&msg);
}

void LaunchpadMiniAsync::setLightArray(const ConstLightPtr light[ROWS][COLS]) {
    for (std::uint8_t y; y < ROWS; y++) {
        for (std::uint8_t x; x < COLS; x++) {
            setLightSingle(Pad(x, y), light[y][x]);
        }
    }
}

void LaunchpadMiniAsync::setLightAll(ConstLightPtr light) {
    for (std::uint8_t y; y < ROWS; y++) {
        for (std::uint8_t x; x < COLS; x++) {
            setLightSingle(Pad(x, y), light);
        }
    }
}

void LaunchpadMiniAsync::setSysExLightSingle(Pad pad, ConstLightPtr light) {
    using namespace std;
    vector<uint8_t> msg = _sysExHeader;
    vector<uint8_t> colorspec = make_pad_colorspec(pad, light);
    msg.push_back(SYSEX_LED_LIGHT);
    msg.insert(msg.end(), colorspec.cbegin(), colorspec.cend());
    msg.push_back(SYSEX_END);
#ifdef LPMINI_DEBUG
    cout << msg << endl;
#endif
    _out.sendMessage(&msg);
}

void LaunchpadMiniAsync::setSysExLightArray(const ConstLightPtr light[ROWS][COLS]) {
    using namespace std;
    vector<std::uint8_t> msg = _sysExHeader;
    msg.push_back(SYSEX_LED_LIGHT);
    vector<uint8_t> colorspec;
    for (uint8_t y = 0; y < ROWS; y++) {
        for (uint8_t x = 0; x < COLS; x++) {
            colorspec = make_pad_colorspec(Pad(x, y), light[y][x]);
            msg.insert(msg.end(), colorspec.cbegin(), colorspec.cend());
        }
    }
    msg.push_back(SYSEX_END);
#ifdef LPMINI_DEBUG
    std::cout << msg << std::endl;
#endif
    _out.sendMessage(&msg);
}

void LaunchpadMiniAsync::setSysExLightAll(ConstLightPtr light) {
    using namespace std;
    vector<std::uint8_t> msg = _sysExHeader;
    msg.push_back(SYSEX_LED_LIGHT);
    vector<uint8_t> colorspec;
    for (uint8_t y = 0; y < ROWS; y++) {
        for (uint8_t x = 0; x < COLS; x++) {
            colorspec = make_pad_colorspec(Pad(x, y), light);
            msg.insert(msg.end(), colorspec.cbegin(), colorspec.cend());
        }
    }
    msg.push_back(SYSEX_END);
#ifdef LPMINI_DEBUG
    std::cout << msg << std::endl;
#endif
    _out.sendMessage(&msg);
}

void LaunchpadMiniAsync::setSysExTextScroll(bool loop, std::uint8_t speed, ConstLightPtr light, const char text[]) {
    std::vector<std::uint8_t> msg = _sysExHeader;
    msg.insert(msg.end(), {SYSEX_TEXT_SCROLL, std::uint8_t(loop), speed});
    std::vector<std::uint8_t> colorspec = make_text_colorspec(light);
    msg.insert(msg.end(), colorspec.cbegin(), colorspec.cend());
    while (*text) {
        msg.push_back(std::uint8_t(*text));
        text++;
    }
    msg.push_back(SYSEX_END);
#ifdef LPMINI_DEBUG
    std::cout << msg << std::endl;
#endif
    _out.sendMessage(&msg);
}

void LaunchpadMiniAsync::setSysExTextScroll(bool loop, std::uint8_t speed, ConstLightPtr light, const std::string &text) {
    std::vector<std::uint8_t> msg = _sysExHeader;
    msg.insert(msg.end(), {SYSEX_TEXT_SCROLL, std::uint8_t(loop), speed});
    std::vector<std::uint8_t> colorspec = make_text_colorspec(light);
    msg.insert(msg.end(), colorspec.cbegin(), colorspec.cend());
    for (const char &c : text) {
        msg.push_back(std::uint8_t(c));
    }
    msg.push_back(SYSEX_END);
#ifdef LPMINI_DEBUG
    std::cout << msg << std::endl;
#endif
}

void LaunchpadMiniAsync::setSysExTextScroll(bool loop, std::uint8_t speed, ConstLightPtr light) {
    std::vector<std::uint8_t> msg = _sysExHeader;
    msg.insert(msg.end(), {SYSEX_TEXT_SCROLL, std::uint8_t(loop), speed});
    std::vector<std::uint8_t> colorspec = make_text_colorspec(light);
    msg.insert(msg.end(), colorspec.cbegin(), colorspec.cend());
    msg.push_back(SYSEX_END);
#ifdef LPMINI_DEBUG
    std::cout << msg << std::endl;
#endif
    _out.sendMessage(&msg);
}

void LaunchpadMiniAsync::setSysExTextScroll(bool loop, std::uint8_t speed) {
    std::vector<std::uint8_t> msg = _sysExHeader;
    msg.insert(msg.end(), {SYSEX_TEXT_SCROLL, std::uint8_t(loop), speed, SYSEX_END});
#ifdef LPMINI_DEBUG
    std::cout << msg << std::endl;
#endif
    _out.sendMessage(&msg);
}

void LaunchpadMiniAsync::setSysExTextScroll(bool loop) {
    std::vector<std::uint8_t> msg = _sysExHeader;
    msg.insert(msg.end(), {SYSEX_TEXT_SCROLL, std::uint8_t(loop), SYSEX_END});
#ifdef LPMINI_DEBUG
    std::cout << msg << std::endl;
#endif
    _out.sendMessage(&msg);
}

void LaunchpadMiniAsync::setSysExTextScrollOff() {
    std::vector<std::uint8_t> msg = _sysExHeader;
    msg.insert(msg.end(), {SYSEX_TEXT_SCROLL, SYSEX_END});
#ifdef LPMINI_DEBUG
    std::cout << msg << std::endl;
#endif
    _out.sendMessage(&msg);
}

void LaunchpadMiniAsync::setSysExPower(bool power) {
    std::vector<std::uint8_t> msg = _sysExHeader;
    msg.insert(msg.end(), {SYSEX_LED_ACTIVE, std::uint8_t(power), SYSEX_END});
#ifdef LPMINI_DEBUG
    std::cout << msg << std::endl;
#endif
    _out.sendMessage(&msg);
}

void LaunchpadMiniAsync::setSysExBrightness(std::uint8_t brightness) {
    std::vector<std::uint8_t> msg = _sysExHeader;
    msg.insert(msg.end(), {SYSEX_BRIGHTNESS, brightness, SYSEX_END});
#ifdef LPMINI_DEBUG
    std::cout << msg << std::endl;
#endif
    _out.sendMessage(&msg);
}

}
